// Diubah ke Arrow Function
const golden = () => {
    console.log("this is golden!!")
} 

golden()

// Menyederhanakan object literal
const literal = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
        return
      }
    }
  }
   
  //Driver Code 
  literal("William", "Imoh").fullName()

// Destructuring
let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell} = newObject;

    // Driver code
    console.log(firstName, lastName, destination, occupation);

// Array Spreading

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west, ...east];
//Driver Code
console.log(combinedArray)

// Template Literals
const planet = "earth";
const view = "glass";
var before = `${planet} ${view}`;

// Driver Code 
console.log(before) 